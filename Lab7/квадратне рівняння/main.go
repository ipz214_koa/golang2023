package main

import (
	"fmt"
	"html/template"
	"math"
	"net/http"
	"strconv"
)

func main() {
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/solve", solveHandler)

	fmt.Println("Server is running on :8080")
	http.ListenAndServe(":8080", nil)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.New("index").Parse(`
		<!DOCTYPE html>
		<html>
		<head>
			<title>Quadratic Equation Solver</title>
		</head>
		<body>
			<h1>Quadratic Equation Solver</h1>
			<form action="/solve" method="post">
				<label for="a">Enter coefficient a:</label>
				<input type="text" name="a" required><br>
				
				<label for="b">Enter coefficient b:</label>
				<input type="text" name="b" required><br>
				
				<label for="c">Enter coefficient c:</label>
				<input type="text" name="c" required><br>
				
				<button type="submit">Solve</button>
			</form>
		</body>
		</html>
	`)
	if err != nil {
		http.Error(w, "Error rendering template", http.StatusInternalServerError)
		return
	}

	tmpl.Execute(w, nil)
}

func solveHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	a, err := strconv.ParseFloat(r.FormValue("a"), 64)
	if err != nil {
		http.Error(w, "Invalid coefficient a", http.StatusBadRequest)
		return
	}

	b, err := strconv.ParseFloat(r.FormValue("b"), 64)
	if err != nil {
		http.Error(w, "Invalid coefficient b", http.StatusBadRequest)
		return
	}

	c, err := strconv.ParseFloat(r.FormValue("c"), 64)
	if err != nil {
		http.Error(w, "Invalid coefficient c", http.StatusBadRequest)
		return
	}

	discriminant := b*b - 4*a*c
	var solutions []float64

	if discriminant > 0 {
		root1 := (-b + math.Sqrt(discriminant)) / (2 * a)
		root2 := (-b - math.Sqrt(discriminant)) / (2 * a)
		solutions = []float64{root1, root2}
	} else if discriminant == 0 {
		root := -b / (2 * a)
		solutions = []float64{root}
	}

	tmpl, err := template.New("result").Parse(`
		<!DOCTYPE html>
		<html>
		<head>
			<title>Quadratic Equation Solver - Result</title>
		</head>
		<body>
			<h1>Quadratic Equation Solver - Result</h1>
			<p>Solutions:</p>
			{{range .}}
				<p>{{.}}</p>
			{{end}}
			<a href="/">Go back</a>
		</body>
		</html>
	`)
	if err != nil {
		http.Error(w, "Error rendering template", http.StatusInternalServerError)
		return
	}

	tmpl.Execute(w, solutions)
}
