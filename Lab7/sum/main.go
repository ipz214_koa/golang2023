package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"strings"
)

func main() {
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/calculate", calculateHandler)

	fmt.Println("Server is running on :8080")
	http.ListenAndServe(":8080", nil)
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.New("index").Parse(`
		<!DOCTYPE html>
		<html>
		<head>
			<title>Array Operations</title>
		</head>
		<body>
			<h1>Array Operations</h1>
			<form action="/calculate" method="post">
				<label for="numbers">Enter numbers (comma-separated):</label>
				<input type="text" name="numbers" required><br>
				
				<button type="submit">Calculate</button>
			</form>
		</body>
		</html>
	`)
	if err != nil {
		http.Error(w, "Error rendering template", http.StatusInternalServerError)
		return
	}

	tmpl.Execute(w, nil)
}

func calculateHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return
	}

	numbersStr := r.FormValue("numbers")
	numberStrings := splitAndTrim(numbersStr, ",")

	var numbers []float64
	for _, numStr := range numberStrings {
		num, err := strconv.ParseFloat(numStr, 64)
		if err != nil {
			http.Error(w, "Invalid number format", http.StatusBadRequest)
			return
		}
		numbers = append(numbers, num)
	}

	sumOfNegatives := calculateSumOfNegatives(numbers)
	product := calculateProduct(numbers)

	result := struct {
		SumOfNegatives float64
		Product        float64
	}{
		SumOfNegatives: sumOfNegatives,
		Product:        product,
	}

	tmpl, err := template.New("result").Parse(`
		<!DOCTYPE html>
		<html>
		<head>
			<title>Array Operations - Result</title>
		</head>
		<body>
			<h1>Array Operations - Result</h1>
			<p>Sum of negatives: {{.SumOfNegatives}}</p>
			<p>Product: {{.Product}}</p>
			<a href="/">Go back</a>
		</body>
		</html>
	`)
	if err != nil {
		http.Error(w, "Error rendering template", http.StatusInternalServerError)
		return
	}

	tmpl.Execute(w, result)
}

func splitAndTrim(s, sep string) []string {
	split := strings.Split(s, sep)
	var result []string
	for _, str := range split {
		trimmed := strings.TrimSpace(str)
		if trimmed != "" {
			result = append(result, trimmed)
		}
	}
	return result
}

func calculateSumOfNegatives(numbers []float64) float64 {
	sum := 0.0
	for _, num := range numbers {
		if num < 0 {
			sum += num
		}
	}
	return sum
}

func calculateProduct(numbers []float64) float64 {
	product := 1.0
	for _, num := range numbers {
		product *= num
	}
	return product
}
