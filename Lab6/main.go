package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// Bank структура для представлення інформації про банк
type Bank struct {
	Name       string
	BankMoney  float64
	Deposit    float64
	Credit     float64
	Clients    []*Client
	clientLock sync.Mutex
}

// Client структура для представлення інформації про клієнта
type Client struct {
	Name          string
	Surname       string
	AccountNumber int
	CDeposit      float64
	CCredit       float64
}

// NewBank конструктор для створення об'єкту банку
func NewBank(name string) *Bank {
	return &Bank{
		Name: name,
	}
}

// NewClient конструктор для створення об'єкту клієнта
func NewClient(name, surname string, accountNumber int) *Client {
	return &Client{
		Name:          name,
		Surname:       surname,
		AccountNumber: accountNumber,
	}
}

// SetCDeposit метод для встановлення суми на депозиті клієнта
func (c *Client) SetCDeposit(amount float64) {
	c.CDeposit = amount
}

// GetCDeposit метод для отримання суми на депозиті клієнта
func (c *Client) GetCDeposit() float64 {
	return c.CDeposit
}

// SetCCredit метод для встановлення суми кредиту, отриманого клієнтом
func (c *Client) SetCCredit(amount float64) {
	c.CCredit = amount
}

// GetCCredit метод для отримання суми кредиту, отриманого клієнтом
func (c *Client) GetCCredit() float64 {
	return c.CCredit
}

// AddClient метод для додавання клієнта до банку
func (b *Bank) AddClient(client *Client) {
	b.clientLock.Lock()
	defer b.clientLock.Unlock()

	b.Clients = append(b.Clients, client)
}

// PrintClientInfoBySurname метод для виведення інформації про клієнта за прізвищем
func (b *Bank) PrintClientInfoBySurname(surname string) {
	b.clientLock.Lock()
	defer b.clientLock.Unlock()

	for _, client := range b.Clients {
		if client.Surname == surname {
			fmt.Printf("Client: %s %s\n", client.Name, client.Surname)
			fmt.Printf("Account Number: %d\n", client.AccountNumber)
			fmt.Printf("Deposit: %.2f\n", client.GetCDeposit())
			fmt.Printf("Credit: %.2f\n", client.GetCCredit())
			fmt.Println("------------------------------")
			return
		}
	}

	fmt.Println("Client not found.")
}

// PrintClientInfoByAccountNumber метод для виведення інформації про клієнта за номером рахунку
func (b *Bank) PrintClientInfoByAccountNumber(accountNumber int) {
	b.clientLock.Lock()
	defer b.clientLock.Unlock()

	for _, client := range b.Clients {
		if client.AccountNumber == accountNumber {
			fmt.Printf("Client: %s %s\n", client.Name, client.Surname)
			fmt.Printf("Account Number: %d\n", client.AccountNumber)
			fmt.Printf("Deposit: %.2f\n", client.GetCDeposit())
			fmt.Printf("Credit: %.2f\n", client.GetCCredit())
			fmt.Println("------------------------------")
			return
		}
	}

	fmt.Println("Client not found.")
}

// ClientBot горутина для бота-клієнта
func ClientBot(bank *Bank, client *Client) {
	for {
		time.Sleep(time.Second)

		// Операції випадковим чином
		operation := rand.Intn(3) // 0 - додати на депозит, 1 - взяти кредит, 2 - повернути кредит

		switch operation {
		case 0:
			depositAmount := rand.Float64() * 1000
			fmt.Printf("%s %s deposited %.2f\n", client.Name, client.Surname, depositAmount)
			client.SetCDeposit(client.GetCDeposit() + depositAmount)
			bank.Deposit += depositAmount

		case 1:
			creditAmount := rand.Float64() * 500
			if client.GetCCredit()+creditAmount <= 1000 {
				fmt.Printf("%s %s took a credit of %.2f\n", client.Name, client.Surname, creditAmount)
				client.SetCCredit(client.GetCCredit() + creditAmount)
				bank.Credit += creditAmount
			} else {
				fmt.Printf("%s %s cannot take more credit. Limit reached.\n", client.Name, client.Surname)
			}

		case 2:
			returnedCredit := rand.Float64() * 500
			if returnedCredit <= client.GetCCredit() {
				fmt.Printf("%s %s returned a credit of %.2f\n", client.Name, client.Surname, returnedCredit)
				client.SetCCredit(client.GetCCredit() - returnedCredit)
				bank.Credit -= returnedCredit
			} else {
				fmt.Printf("%s %s cannot return more credit than taken.\n", client.Name, client.Surname)
			}
		}
	}
}

func main() {
	bank := NewBank("MyBank")

	// Створення банку
	client1 := NewClient("John", "Doe", 123)
	client2 := NewClient("Jane", "Smith", 456)

	bank.AddClient(client1)
	bank.AddClient(client2)

	// Створення ботів-клієнтів
	go ClientBot(bank, client1)
	go ClientBot(bank, client2)

	// Консольне меню
	for {
		fmt.Println("1. Print Client Info by Surname")
		fmt.Println("2. Print Client Info by Account Number")
		fmt.Println("3. Exit")

		var choice int
		fmt.Print("Enter your choice: ")
		fmt.Scan(&choice)

		switch choice {
		case 1:
			var surname string
			fmt.Print("Enter client surname: ")
			fmt.Scan(&surname)
			bank.PrintClientInfoBySurname(surname)

		case 2:
			var accountNumber int
			fmt.Print("Enter account number: ")
			fmt.Scan(&accountNumber)
			bank.PrintClientInfoByAccountNumber(accountNumber)

		case 3:
			fmt.Println("Exiting...")
			return

		default:
			fmt.Println("Invalid choice. Try again.")
		}
	}
}
