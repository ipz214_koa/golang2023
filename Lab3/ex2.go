package main

import (
	"fmt"
)

const (
	a = 1103515245
	c = 12345
	m = 1 << 31
	n = 100
	K = 10000
)

func main() {
	// Генерування послідовності псевдовипадкових дійсних чисел
	randomNumbers := generateRandomRealSequence()

	// Друк згенерованої послідовності
	fmt.Println(randomNumbers)
}

func generateRandomRealSequence() []float64 {
	var result []float64
	x := 42 // початкове значення

	for i := 0; i < K; i++ {
		x = (a*x + c) % m
		randomValue := float64(x) / float64(m) * float64(n)
		result = append(result, randomValue)
	}

	return result
}
