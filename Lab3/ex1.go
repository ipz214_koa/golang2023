package main

import (
	"fmt"
	"math"
)

const (
	a = 1103515245
	c = 12345
	m = 1 << 31
	n = 100
	K = 10000
)

func main() {
	// Генерування послідовності псевдовипадкових чисел
	randomNumbers := generateRandomSequence()

	// Обробка отриманої послідовності
	frequencies := make(map[int]int)
	sum := 0.0
	sumSquared := 0.0

	for _, num := range randomNumbers {
		frequencies[num]++
		sum += float64(num)
		sumSquared += math.Pow(float64(num), 2)
	}

	// Розрахунок частоти, імовірності, математичного сподівання, дисперсії та середньоквадратичного відхилення
	fmt.Printf("%-10s%-15s%-20s\n", "Значення", "Частота", "Імовірність")
	for i := 0; i < n; i++ {
		frequency := float64(frequencies[i]) / float64(K)
		probability := frequency / float64(n)
		fmt.Printf("%-10d%-15f%-20f\n", i, frequency, probability)
	}

	mean := sum / float64(K)
	variance := (sumSquared / float64(K)) - math.Pow(mean, 2)
	stdDev := math.Sqrt(variance)

	fmt.Printf("\nМатематичне сподівання: %f\n", mean)
	fmt.Printf("Дисперсія: %f\n", variance)
	fmt.Printf("Середньоквадратичне відхилення: %f\n", stdDev)
}

func generateRandomSequence() []int {
	var result []int
	x := 42 // початкове значення

	for i := 0; i < K; i++ {
		x = (a*x + c) % m
		result = append(result, x%n)
	}

	return result
}
