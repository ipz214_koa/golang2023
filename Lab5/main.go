package main

import (
	"fmt"
)

// Currency структура, що представляє інформацію про валюту
type Currency struct {
	Name   string
	ExRate float64
}

// Product структура, що представляє інформацію про товар
type Product struct {
	Name     string
	Price    float64
	Cost     Currency
	Quantity int
	Producer string
	Weight   float64
}

// GetPriceIn повертає ціну одиниці товару в гривнях
func (p Product) GetPriceIn() float64 {
	return p.Price * p.Cost.ExRate
}

// GetTotalPrice повертає загальну вартість усіх наявних на складі товарів даного виду
func (p Product) GetTotalPrice() float64 {
	return float64(p.Quantity) * p.GetPriceIn()
}

// GetTotalWeight повертає загальну вагу усіх товарів на складі даного виду
func (p Product) GetTotalWeight() float64 {
	return float64(p.Quantity) * p.Weight
}

// ReadProductsArray читає з клавіатури дані і повертає множину об’єктів типу Product
func ReadProductsArray() []Product {
	var n int
	fmt.Print("Введіть кількість товарів: ")
	fmt.Scan(&n)

	products := make([]Product, n)

	for i := 0; i < n; i++ {
		var product Product
		fmt.Printf("Введіть дані для товару %d:\n", i+1)
		fmt.Print("Назва товару: ")
		fmt.Scan(&product.Name)
		fmt.Print("Ціна товару: ")
		fmt.Scan(&product.Price)
		fmt.Print("Валюта (назва та курс через пробіл): ")
		fmt.Scan(&product.Cost.Name, &product.Cost.ExRate)
		fmt.Print("Кількість товару на складі: ")
		fmt.Scan(&product.Quantity)
		fmt.Print("Виробник товару: ")
		fmt.Scan(&product.Producer)
		fmt.Print("Вага одиниці товару: ")
		fmt.Scan(&product.Weight)

		products[i] = product
	}

	return products
}

// PrintProduct приймає тип Product і виводить його на екран
func PrintProduct(p Product) {
	fmt.Printf("Назва: %s\n", p.Name)
	fmt.Printf("Ціна: %.2f %s\n", p.Price, p.Cost.Name)
	fmt.Printf("Кількість на складі: %d\n", p.Quantity)
	fmt.Printf("Виробник: %s\n", p.Producer)
	fmt.Printf("Вага одиниці товару: %.2f\n", p.Weight)
	fmt.Println()
}

// PrintProducts приймає множину типу Product і виводить її на екран
func PrintProducts(products []Product) {
	for _, p := range products {
		PrintProduct(p)
	}
}

// GetProductsInfo приймає множину типу Product і повертає через вихідні параметри найдешевший та найдорожчий товар
func GetProductsInfo(products []Product) (cheapest Product, mostExpensive Product) {
	if len(products) > 0 {
		cheapest, mostExpensive = products[0], products[0]

		for _, p := range products {
			if p.GetPriceIn() < cheapest.GetPriceIn() {
				cheapest = p
			}
			if p.GetPriceIn() > mostExpensive.GetPriceIn() {
				mostExpensive = p
			}
		}
	}

	return cheapest, mostExpensive
}

func main() {
	// Читання даних з клавіатури і створення масиву товарів
	products := ReadProductsArray()

	// Виведення інформації про товари
	fmt.Println("Інформація про товари:")
	PrintProducts(products)

	// Отримання і виведення інформації про найдешевший та найдорожчий товар
	cheapest, mostExpensive := GetProductsInfo(products)
	fmt.Println("Найдешевший товар:")
	PrintProduct(cheapest)
	fmt.Println("Найдорожчий товар:")
	PrintProduct(mostExpensive)
}
