package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	var ukrainianChar rune = 'Ї'
	fmt.Printf("Code '%c' - %d\n", ukrainianChar, ukrainianChar)

	// Пояснение назначения типа "rune"
	// Тип rune в Go призначений для представлення Unicode-символів.
	// Він займає 4 байта и може представляти будь-який символ з Unicode.
}
