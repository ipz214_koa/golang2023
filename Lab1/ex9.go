package main

import "fmt"

func main() {
	var first, second bool //В мові програмування Go типу bool дефолтне значення - false.
	var third bool = true
	fourth := !third // !third  означає не third, тобто не true a false
	var fifth = true

	fmt.Println("first  = ", first)       // false
	fmt.Println("second = ", second)      // false
	fmt.Println("third  = ", third)       // true
	fmt.Println("fourth = ", fourth)      // false
	fmt.Println("fifth  = ", fifth, "\n") // true

	fmt.Println("!true  = ", !true)        // false      !true - Логічне НІ. Результат: false.
	fmt.Println("!false = ", !false, "\n") // true       !false - Логічне НІ. Результат: true.

	//Логічне І.
	fmt.Println("true && true   = ", true && true)         // true
	fmt.Println("true && false  = ", true && false)        // false
	fmt.Println("false && false = ", false && false, "\n") // false

	//Логічне або.
	fmt.Println("true || true   = ", true || true)         // true
	fmt.Println("true || false  = ", true || false)        // true
	fmt.Println("false || false = ", false || false, "\n") // false

	//Операції порівняння
	fmt.Println("2 < 3  = ", 2 < 3)        // true    //два менше трьох
	fmt.Println("2 > 3  = ", 2 > 3)        // false   //два не є більше трох
	fmt.Println("3 < 3  = ", 3 < 3)        // false   //три не є менше трох
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true    //три менше або дорівнює трьом
	fmt.Println("3 > 3  = ", 3 > 3)        // false   //три не є більше трьох
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true    //три більше або дорівнює трьом
	fmt.Println("2 == 3 = ", 2 == 3)       // false   //два не дорівнює трьом
	fmt.Println("3 == 3 = ", 3 == 3)       // true    //три дорівнює трьом
	fmt.Println("2 != 3 = ", 2 != 3)       // true    //два не дорівнює трьом
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false   //три дорівнює трьом

	//Задание.
	//1. Пояснить результаты операций

}
