package main

//Импорт нескольких пакетов
import (
	"fmt"
	"math"
)

func main() {
	var defaultFloat float32
	var defaultDouble float64 = 5.5

	fmt.Println("defaultfloat       = ", defaultFloat)
	fmt.Printf("defaultDouble (%T) = %f\n\n", defaultDouble, defaultDouble)

	fmt.Println("MAX float32        = ", math.MaxFloat32)
	fmt.Println("MIN float32        = ", math.SmallestNonzeroFloat32, "\n")

	fmt.Println("MAX float64        = ", math.MaxFloat64)
	fmt.Println("MIN float64        = ", math.SmallestNonzeroFloat64, "\n")

	//Задание.
	//1. Создайте переменные разных типов, используя краткую запись и инициализацию по-умолчанию. Результат вывести

	// Инициализация переменных с использованием краткой записи
	shortInt := 42
	shortString := "Hello, Golang!"
	shortBool := true

	// Вывод результатов
	fmt.Println("Краткая запись и инициализация по-умолчанию\n")
	fmt.Printf("shortInt    (%T) = %d\n", shortInt, shortInt)
	fmt.Printf("shortString (%T) = %s\n", shortString, shortString)
	fmt.Printf("shortBool   (%T) = %t\n\n", shortBool, shortBool)
}
